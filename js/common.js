var $c = {
	siteUrl : "http://yourongcn.sinaapp.com",
	getDom : function(id) {

		return document.getElementById(id);
	},
	getInterface : function(module, action, params) {//获取接口地址

		//构造url
		if (params == undefined) {

			targetUrl = this.siteUrl + "/index.php?m=" + module + "&a=" + action;
		} else {

			targetUrl = this.siteUrl + "/index.php?m=" + module + "&a=" + action + "&" + params
		}

		console.log(targetUrl);
		return targetUrl;

	},
	openUrl : function(obj) {
		var url;
		if (typeOf(obj) == Object) {
			url = obj.attributes["url"].value;
		} else if (typeOf(obj) == String) {
			url = obj;

		}
		window.open(url);
	},
	isPc : function() {

		var userAgentInfo = navigator.userAgent;
		var Agents = new Array("Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod");
		var flag = true;
		for (var v = 0; v < Agents.length; v++) {
			if (userAgentInfo.indexOf(Agents[v]) > 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}
}

// /////平滑锚点
// $(document).ready(function() {
// $(".link").click(function() {
// console.log("link clicked");
//
// return false;
// });
// });

///客户案例左右滑动按钮
$(document).ready(function() {
	$("#toleft").mouseover(function() {
		$(this).children("img").stop().animate({
			opacity : "0.5"
		}, "fast");
	});
	$("#toleft").mouseout(function() {
		$(this).children("img").stop().animate({
			opacity : "1"
		}, "fast");
	});

	$("#toright").mouseover(function() {
		$(this).children("img").stop().animate({
			opacity : "0.5"
		}, "fast");
	});
	$("#toright").mouseout(function() {
		$(this).children("img").stop().animate({
			opacity : "1"
		}, "fast");
	});

	//点击事件
	var windowWidth = parseInt(document.body.clientWidth);
	windowWidth = 0 - windowWidth;
	$("#works").css("margin-left", Math.abs(windowWidth) + "px");

	$("#toleft img").click(function() {
		$("#inc").animate({
			marginLeft : windowWidth + "px"
		}, "slow");
		$("#works").fadeIn();
		$("#works").animate({
			marginLeft : "0px"
		}, "slow");

		$(this).animate({
			opacity : "0.5"
		}, "slow");
		$("#toright img").animate({
			opacity : "1"
		}, "slow");
	});
	$("#toright img").click(function() {
		$("#inc").animate({
			marginLeft : "0px"
		}, "slow");
		$("#works").fadeOut();
		$("#works").animate({
			marginLeft : Math.abs(windowWidth) + "px"
		}, "slow");
		$(this).animate({
			opacity : "0.5"
		}, "slow");
		$("#toleft img").animate({
			opacity : "1"
		}, "slow");
	});

});

$(document).ready(function() {
	$("#media").hover(function() {
		$("#media").stop().animate({
			backgroundColor : "rgb(113,32,43)"
		}, "slow");

	});
	$("#media").mouseout(function() {
		$("#media").stop().animate({
			backgroundColor : "rgb(174,47,64)"
		}, "slow");

	});

});



$(document).ready(function(){
	if(!$c.isPc()){
		$("#topFrame").css("width","1024px");
		$("#product").css("width","1024px");
		$("#sliderbox").css("width","1024px");
		$("#brand").css("width","1024px");
		$("#media").css("width","1024px");
		$("#zhaopin").css("width","1024px");
		$("#contact").css("width","1024px");
		$("#footer").css("width","1024px");
		$("#custom_cases").css("width","1024px");
	}
});

///动态按钮
//按钮初始化
$(document).ready(function() {

	var btns = $(".btn");
	var btnNum = btns.length;
	for (var i = 0; i < btnNum; i++) {
		var btnUrl = $(btns[i]).attr("href");
		var target = $(btns[i]).attr("target");
		if (target == null) {
			console.log("target 未定义");
			target = "";
		} else {
			target = "target=\"" + target + "\"";
		}
		console.log(target);
		var linklabel = "";

		var linkText = $(btns[i]).text();
		$(btns[i]).html("");
		$(btns[i]).append("<a href=\"" + btnUrl + "\">" + linkText + "</a>");
		$(btns[i]).append("<div class=\"linkbg\"></div>");

		if (btnUrl == "none") {
			linklabel = "<a  " + target + " class=\"btnlink\" style=\"color:#ffffff;\">" + linkText + "</a>";
		} else {
			linklabel = "<a  " + target + " class=\"btnlink\" style=\"color:#ffffff;\" href=\"" + btnUrl + "\">" + linkText + "</a>"
		};

		$(btns[i]).append(linklabel);
		var marginLeft = parseInt($(btns[i]));
		marginLeft = 0 - marginLeft;
		$(btns[i]).children(".btnlink").css("marginLeft", marginLeft + "px");

	}

});

///客服代码
$(document).ready(function() {

	$("#qqkefu").mouseover(function() {
		console.log("over");
		$("#qqkefu").css("background-color", "#6a6969");
		$("#qqkefu").animate({
			right : '0px'
		});
	});
	$("#qqkefu").mouseleave(function() {
		console.log("over");
		$("#qqkefu").css("background-color", "#767676");
		$("#qqkefu").animate({
			right : '-150px'
		});
	});

	$("#telkefu").mouseover(function() {
		console.log("over");
		$("#telkefu").css("background-color", "#6a6969");
		$("#telkefu").animate({
			right : '0px'
		});
	});
	$("#telkefu").mouseleave(function() {
		console.log("over");
		$("#telkefu").css("background-color", "#767676");
		$("#telkefu").animate({
			right : '-150px'
		});
	});
});

$(document).ready(function() {
	$(".btn").hover(function() {
		var btn = $(this);
		var linkbg = $(this).children(".linkbg");
		var btnlink = $(this).children(".btnlink");

		var height = parseInt(btn.height());
		var width = parseInt(btn.width()) - 8;
		linkbg.css("width", width + "px");
		btnlink.css("color", "#ae2f40");
		linkbg.stop().animate({
			height : height + "px"
		});

	});
	$(".btn").mouseout(function() {
		var linkbg = $(this).children(".linkbg");
		var btnlink = $(this).children(".btnlink");
		btnlink.css("color", "#ffffff");
		linkbg.stop().animate({
			height : "0px"
		});

	});

});


//servicebox
$(document).ready(function(){
	
	$(".serBox").hover(function () {
	 $(this).children().stop(false,true);
	 $(this).children(".serBoxOn").fadeIn("slow");
     $(this).children(".pic1").animate({right: -110},400);
     $(this).children(".pic2").animate({left: 41},400);
     $(this).children(".txt1").animate({left: -240},400);
     $(this).children(".txt2").animate({right: 0},400);	
},function () {
	 $(this).children().stop(false,true);
	 $(this).children(".serBoxOn").fadeOut("slow");
	 $(this).children(".pic1").animate({right:41},400);
     $(this).children(".pic2").animate({left: -110},400);
     $(this).children(".txt1").animate({left: 0},400);
     $(this).children(".txt2").animate({right: -240},400);	
});

	
});
